//WHILE

console.log('LOOPING PERTAMA');
var angka = 2;
while(angka<=20){
    console.log(angka + ' - I love coding');
    angka+=2;
}
console.log('LOOPING KEDUA');
var angka2=20;
while(angka2>=2){
    angka2-=2;
    console.log(angka2 + ' - I will become a mobile Developer')
}

//FOR

for(var angka3=1; angka3<=20; angka3++){
    if((angka3%3)===0){
        console.log(angka3+' - I Love Coding');
    }
    else if((angka3%2)===0){
        console.log(angka3+' - Berkualitas');
    }
    else{
        console.log(angka3+' - Santai');
    }
}

//Looping Persegi Panjang
var tanda="########";
var jumlah=4;
for(i=0; i<jumlah; i++){
    console.log(tanda)
}


//Looping Tangga
var tanda="#";
var hasil="";
for(i=0; i<=7; i++){
    for(j=0; j<=7; j++){
        console.log(hasil=hasil+tanda);
    }
}


//Looping Catur
var hitam = "#"
var putih = " ";
var hasil = "";
var num = 8;
for (i = 0; i < num; i++) 
{

    for (j = 0; j < num; j++) 
    {
        if (i % 2 == 1) 
        {

            if (j % 2 == 0) { 
                hasil = hasil + hitam; 
            }
            else 
            {
                hasil = hasil + putih;
            }

        }
        else
        {
            if (j % 2 == 0) {
                hasil = hasil + putih; 
            }
            else 
            {
                hasil = hasil + hitam;
            }
        }
    }
    hasil = hasil + " \n ";
    console.log(hasil);
    hasil = " ";
}